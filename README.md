# react/partial

Partial function application

[![PHPPackages Rank](http://phppackages.org/p/react/partial/badge/rank.svg)](http://phppackages.org/p/react/partial)

* [*Pro Functional Php Programming Application Development Strategies for Performance Optimization, Concurrency, Testability, and Code Brevity*](https://www.worldcat.org/title/pro-functional-php-programming-application-development-strategies-for-performance-optimization-concurrency-testability-and-code-brevity)
* [*PHP Reactive Programming*](https://www.worldcat.org/title/php-reactive-programming)
* [*Functional programming*](https://en.wikipedia.org/wiki/Functional_programming)
* [Functional Programming in PHP](https://google.com/search?q=Functional+Programming+in+PHP)